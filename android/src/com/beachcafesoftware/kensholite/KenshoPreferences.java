package com.beachcafesoftware.kensholite;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;

//@SuppressWarnings("deprecation")
public class KenshoPreferences extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    String SHARED_PREFS_NAME = "cube2settings";

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        getPreferenceManager().setSharedPreferencesName(
                   SHARED_PREFS_NAME);

         addPreferencesFromResource(R.xml.cube2_settings);
         getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(
                this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(
                this);
        super.onDestroy();
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                          String key) {
        KenshoLite.prefsChanged = true;
    }
}