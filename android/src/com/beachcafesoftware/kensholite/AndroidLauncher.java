package com.beachcafesoftware.kensholite;

import android.app.WallpaperInfo;
import android.app.WallpaperManager;
import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

// this application is never installed on the android device, because the Activity is commented out in the AndroidManifest.xml.
// only the live wallpaper Service is installed.

public class AndroidLauncher extends AndroidApplication {   
	@Override
	protected void onCreate (Bundle savedInstanceState) {   
		super.onCreate(savedInstanceState);
	}
}
