package com.beachcafesoftware.kensholite;

import java.util.Random;

/**
 * Created by ubuntu on 18/12/17.
 */

public class ForestColorScheme implements ColorScheme {

    private final Random rnd;


    public ForestColorScheme() {
        rnd = new Random();
    }

    @Override
    public int[] getColor() {

        float randomValue = rnd.nextFloat();

        int r;
        int g;
        int b;

        // water
        if(randomValue < 0.1) {
            b = rnd.nextInt(256);
            g = b / 2 + rnd.nextInt(1 + b / 2);
            r = rnd.nextInt(1 + g);
        }
        // dirt
        else if(randomValue < 0.2) {
            r = rnd.nextInt(96);
            g = r / 2 + rnd.nextInt(1 + r / 2);
            b = rnd.nextInt(1 + g);
        }
        // grass
        else {
            g = rnd.nextInt(256);
            b = g / 2 + rnd.nextInt(1 + g / 2);
            r = rnd.nextInt(1 + b);
        }
        return new int[] { r, g, b };
    }


}
