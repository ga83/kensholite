package com.beachcafesoftware.kensholite;

/**
 * Created by ubuntu on 21/04/18.
 */

public interface KenshoRenderer {

    String getShader();    // returns shader in string format

}
