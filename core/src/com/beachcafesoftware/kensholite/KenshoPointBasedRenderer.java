package com.beachcafesoftware.kensholite;

/**
 * Created by ubuntu on 21/04/18.
 */

public abstract class KenshoPointBasedRenderer implements KenshoRenderer {

    // currently, we are instantiating objects of this class in the main app, not its parent interface.
    // currently we don't have all the drawing functionality in here that we should.
    // we are just generating the shader code body, for a performance hotfix.
    // and for the sake of performance, it only stores the performance-related information needed,
    // which is the number of sinks and spouts respectively.
    // but later on, the main app will instantiate a KenshoRenderer interface.
    // the renderer will encapsulate more data, such as current angle (which only applies to rotating points),
    // number of petals (again, only applies to some shaders).
    // probably most of the variables in the main class will be moved into here, or subclasses (eg angle, petals).


    // for now, these only refer to the main class' copies of these.
    // later on, we can have a different set of spouts and sinks for each renderer (layer),
    // so that they can be in different spots, or even different in number, which creates a different visual effect.
    // but we're not even using them yet so they are commented out.
    // private PointKensho[] pointsKensho;
    // private float[] pointsKenshoFloat;
    // private PointKensho[] pointsKenshoSink;
    // private float[] pointsKenshoSinkFloat;

    // used primarily because we can save a lot of shader gpu time,
    // by doing less calculations when there are fewer spouts and sinks
    protected int numSinks;
    protected int numSpouts;
    protected boolean quantumEffect;
    protected boolean grainEffect;
    protected boolean hueShiftEffect;
    protected boolean fuzzyEdgesEffect;


    public KenshoPointBasedRenderer(int numSpouts, int numSinks, boolean quantumEffect, boolean grainEffect, boolean hueShiftEffect, boolean fuzzyEdgesEffect) {
        this.numSpouts = numSpouts;
        this.numSinks = numSinks;
        this.quantumEffect = quantumEffect;
        this.grainEffect = grainEffect;
        this.hueShiftEffect = hueShiftEffect;
        this.fuzzyEdgesEffect = fuzzyEdgesEffect;
    }

    protected String getNoiseValue() {
        return
                "float seed = 1.0;\n" +
//                        "seed += heightFunctionSine(vec2(nsp1x,nsp1y), noise_seed_phase1, 1.0);\n" +
                        "seed += heightFunctionSine(vec2(nsp2x,nsp2y), noise_seed_phase2, 1.0);\n" +
                        "seed += heightFunctionSine(vec2(nsp3x,nsp3y), noise_seed_phase3, 1.0);\n" +
                        "lowp float noise_value = gold_noise(vec2(0.03 * v_texCoords[0], 0.03 * v_texCoords[1]), seed);\n";
    }

    protected String getFuzzyEdges() {
        String a;
        a = "float positive = -1.1 + float(noise_value > 0.5) * 2.3;\n" +
                "lum += (abs(noise_value) * abs(noise_value)) * 0.0062 * positive ;\n";
        return a;
    }

    protected String getQuantum() {
        return
                "float largest;\n" +
                        "largest = max(gl_FragColor[0], gl_FragColor[1]);\n" +
                        "largest = max(largest, gl_FragColor[2]);\n" +
                        "float scale = 1.0 / largest;\n" +

                        "float display = float(largest > (noise_value * 1.0))   ;\n" +
                        "gl_FragColor[0] = gl_FragColor[0] * scale * display;\n" +
                        "gl_FragColor[1] = gl_FragColor[1] * scale * display;\n" +
                        "gl_FragColor[2] = gl_FragColor[2] * scale * display;\n" +
                        "gl_FragColor[3] = 1.0;\n";
    }

    protected String getGrain() {
        return
                        "float reduction = noise_value * 3.0;\n" +
                        "gl_FragColor[0] = gl_FragColor[0] * (1.0 - reduction);\n" +
                        "gl_FragColor[1] = gl_FragColor[1] * (1.0 - reduction);\n" +
                        "gl_FragColor[2] = gl_FragColor[2] * (1.0 - reduction);\n" +
                        "gl_FragColor[3] = 1.0;\n";
    }

    protected String getHueShift() {
        return
                "float hueAdjust = (-0.5 + v_texCoords[1]) * 1.04;\n" +     // arbitrary multiplier
                "vec3 hueShifted = hueShift(vec3(gl_FragColor[0],gl_FragColor[1],gl_FragColor[2]), hueAdjust);\n" +
                "gl_FragColor[0] = hueShifted[0];\n" +
                "gl_FragColor[1] = hueShifted[1];\n" +
                "gl_FragColor[2] = hueShifted[2];\n";
    }

    protected String getHeightFunctionSine() {
        return
                "float heightFunctionSine(vec2 position, float phase, float amplitude)\n" +
                        "{\n" + "lowp float dx = (position[0] - v_texCoords[0]);\n" +
                        "lowp float dy = (position[1] - v_texCoords[1]) * landscape_aspect_ratio;\n" +
                        "lowp float angle = atan(dy / dx) + addAngle;\n" +
                        "return ((sin(phase + 1000.0 * inversesqrt(dx * dx + dy * dy))) * amplitude);\n" +
                        "}";
    }

 }
