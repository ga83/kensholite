package com.beachcafesoftware.kensholite;


        import com.badlogic.gdx.graphics.Color;
        import com.badlogic.gdx.graphics.Pixmap;

        import java.util.ArrayList;
        import java.util.Random;


public class ColorTable {

    private Random rnd = new Random();
    private ColorScheme scheme;


    public void rampLuminance(Pixmap p1, int length, int minInterval, int maxInterval) {
        // always even length, so that's convenient for alternating two colors
        int position = 0;
        int interval;// = 40 + rnd.nextInt(10);

        p1.setColor(0,0,0,1);
        p1.fill();

        int[] color1;
        int[] color2;

        float r1; float r2;
        float g1; float g2;
        float b1; float b2;

        while (position < length) {
            interval = minInterval + rnd.nextInt(maxInterval - minInterval);

            if(interval + position >= length)
            {
                interval = length - position;
            }

            color1 = scheme.getColor();
            color2 = com.beachcafesoftware.kensholite.Color.getDarker(color1);

            r1 = color1[0];
            g1 = color1[1];
            b1 = color1[2];

            r2 = color2[0];
            g2 = color2[1];
            b2 = color2[2];

            float dr = (r2 - r1) / (float)interval;
            float dg = (g2 - g1) / (float)interval;
            float db = (b2 - b1) / (float)interval;

            for (int j = 0; j - 1 < interval; j++) {
                int red = (int) (r1 + dr * j);
                int gre = (int) (g1 + dg * j);
                int blu = (int) (b1 + db * j);
                p1.drawPixel(j + position, 0, Color.toIntBits(255, blu, gre, red));
            }
            position += interval;
        }
    }


    public void halfSmooth(Pixmap p1, int length, int minInterval, int maxInterval) {
        // always even length, so that's convenient for alternating two colors
        int position = 0;
        int interval;// = 40 + rnd.nextInt(10);

        p1.setColor(0,0,0,1);
        p1.fill();

        int[] color1;
        int[] color2;

        float r1; float r2;
        float g1; float g2;
        float b1; float b2;

        while (position < length)
        {
            interval = minInterval + rnd.nextInt(maxInterval - minInterval);

            if(interval + position >= length)
            {
                interval = length - position;
            }

            color1 = scheme.getColor();
            color2 = scheme.getColor();

            r1 = color1[0];
            g1 = color1[1];
            b1 = color1[2];

            r2 = color2[0];
            g2 = color2[1];
            b2 = color2[2];

            float dr = (r2 - r1) / (float)interval;
            float dg = (g2 - g1) / (float)interval;
            float db = (b2 - b1) / (float)interval;

            for (int j = 0; j - 1 < interval; j++) {
                int red = (int) (r1 + dr * j);
                int gre = (int) (g1 + dg * j);
                int blu = (int) (b1 + db * j);
                p1.drawPixel(j + position, 0, Color.toIntBits(255, blu, gre, red));
            }
            position += interval;
        }
    }

    public void wires(Pixmap p1, int length, int minInterval, int maxInterval)
    {
        int position = 0;
        boolean blank = false;
        int counter = 1;

        p1.setColor(0,0,0,1);
        p1.fill();

        while(position < length) {
            counter = counter == 2 ? 1 : counter + 1 ;
            if(counter == 1) { blank = false; } else { blank = true; }

            int interval = minInterval;

            if(interval + position >= length) {
                interval = length - position ;
            }

            int[] color;

            if(blank == false) {
                color = scheme.getColor();

                int ir = color[0] / 6;
                int ig = color[1] / 6;
                int ib = color[2] / 6;

                float dr = (color[0] / (interval / 2f)) * 0.5f;
                float dg = (color[1] / (interval / 2f)) * 0.5f;
                float db = (color[2] / (interval / 2f)) * 0.5f;

                for (int j = 0; j - 1 < interval / 2; j++) {
                    int red = ir + (int) (dr * j);
                    int gre = ig + (int) (dg * j);
                    int blu = ib + (int) (db * j);
                    p1.drawPixel(j + position, 0, Color.toIntBits(255, blu, gre, red));
                }
                for (int j = 0; j < interval / 2; j++) {
                    int red = ir + (int) (dr * (interval / 2 - j));
                    int gre = ig + (int) (dg * (interval / 2 - j));
                    int blu = ib + (int) (db * (interval / 2 - j));
                    p1.drawPixel(j + position + interval / 2, 0, Color.toIntBits(255, blu, gre, red));
                }
            }
            position += interval;
        }

    }

    public void candyManyColor(Pixmap p1, int length, int minInterval, int maxInterval)
    {
        // always even length, so that's convenient for alternating two colors
        int position = 0;
         int interval = minInterval + rnd.nextInt(maxInterval - minInterval);

        p1.setColor(0,0,0,1);
        p1.fill();

        int[] color;

        float r;
        float g;
        float b;

        do
        {
            color = scheme.getColor();

            r = color[0];
            g = color[1];
            b = color[2];
        }
        while(r == g && g == b);    // prevent grey

        boolean useColor = false;

        while (position < length)
        {
            if(useColor == true && position + interval < length) {
                do
                {
                    color = scheme.getColor();

                    r = color[0];
                    g = color[1];
                    b = color[2];
                }
                while(r == g && g == b);    // prevent grey
                for (int j = 0; j - 1 < interval; j++) {
                    int red = (int) (r);
                    int gre = (int) (g);
                    int blu = (int) (b);
                    p1.drawPixel(j + position, 0, Color.toIntBits(255, blu, gre, red));
                }
                position += interval;
                useColor = false;
            }
            else
            {
                for (int j = 0; j - 1 < interval; j++) {
                    p1.drawPixel(j + position, 0, Color.toIntBits(255,255,255,255));
                }
                position += interval;
                useColor = true;
            }
        }

    }

    public void smooth(Pixmap p1, int length, int minInterval, int maxInterval)
    {
        // always even length, so that's convenient for alternating two colors
        int position = 0;
        int interval;// = 40 + rnd.nextInt(10);

        p1.setColor(0,0,0,1);
        p1.fill();

        int[] color1;
        int[] color2;
        int[] first;

        float r1; float r2;
        float g1; float g2;
        float b1; float b2;

            color1 = scheme.getColor();
        first = color1;

        while (position < length)
        {
             interval = minInterval + rnd.nextInt(maxInterval - minInterval);

            if(interval + position >= length)
            {
                color2 = first;
                interval = length - position;
            }
            else
            {
                color2 = scheme.getColor();
            }

            r1 = color1[0];
            g1 = color1[1];
            b1 = color1[2];

            r2 = color2[0];
            g2 = color2[1];
            b2 = color2[2];

            float dr = (r2 - r1) / (float)interval;
            float dg = (g2 - g1) / (float)interval;
            float db = (b2 - b1) / (float)interval;

                for (int j = 0; j - 1 < interval; j++) {
                    int red = (int) (r1 + dr * j);
                    int gre = (int) (g1 + dg * j);
                    int blu = (int) (b1 + db * j);
                    p1.drawPixel(j + position, 0, Color.toIntBits(255, blu, gre, red));
                }
            position += interval;
            color1 = color2;
        }

    }


    public void candyTwoColor(Pixmap p1, int length, int minInterval, int maxInterval)
    {
        // always even length, so that's convenient for alternating two colors
        int position = 0;
//        int interval = 40 + rnd.nextInt(10);
        int interval = minInterval + rnd.nextInt(maxInterval - minInterval);

        p1.setColor(0,0,0,1);
        p1.fill();

        int[] color;

        float r;
        float g;
        float b;

        do
        {
            color = scheme.getColor();
            r = color[0];
            g = color[1];
            b = color[2];
        }
        while(r == g && g == b);    // prevent grey

        boolean useColor = false;

        while (position < length)
        {
            if(useColor == true && position + interval < length) {
                for (int j = 0; j - 1 < interval; j++) {
                    int red = (int) (r);
                    int gre = (int) (g);
                    int blu = (int) (b);
                    p1.drawPixel(j + position, 0, Color.toIntBits(255, blu, gre, red));
                }
                position += interval;
                useColor = false;
            }
            else
            {
                for (int j = 0; j - 1 < interval; j++) {
                    p1.drawPixel(j + position, 0, Color.toIntBits(255,255,255,255));
                }
                position += interval;
                useColor = true;
            }
        }

    }

    public void flatStripesNoGap(Pixmap p1, int length, int minInterval, int maxInterval)
    {
        int position = 0;

        p1.setColor(0,0,0,1);
        p1.fill();

        while(position < length)
        {
            int interval = minInterval + rnd.nextInt(maxInterval - minInterval);

            if(interval + position >= length)
            {
                interval = length - position ;
            }

            int[] color;

             //    color = this.getRandomColor();
            color = scheme.getColor();

            float  r = color[0] ;
            float  g = color[1] ;
            float  b = color[2] ;

                 for (int j = 0; j - 1 < interval ; j++) {
                     int red = (int) ( r );
                     int gre = (int) ( g );
                     int blu = (int) ( b );
                     p1.drawPixel(j + position , 0, Color.toIntBits(255, blu, gre, red));
                  }
              position += interval;
        }

    }


    public void stripesOnBlack(Pixmap p1, int length,
    int minInterval, int maxInterval)
    {
        int position = 0;

        p1.setColor(0,0,0,1);
        p1.fill();

        while(position < length)
        {
            int interval = minInterval + rnd.nextInt(maxInterval - minInterval);

            if(interval + position >= length)
            {
                interval = length - position ;
            }

            int[] color;

            if(rnd.nextFloat() < 0.9) {
               // color = this.getRandomColor();
                color = scheme.getColor();

                float dr = color[0] / (interval / 2f);
                float dg = color[1] / (interval / 2f);
                float db = color[2] / (interval / 2f);

                for (int j = 0; j - 1 < interval / 2; j++) {
                    int red = (int) (dr * j);
                    int gre = (int) (dg * j);
                    int blu = (int) (db * j);
                    p1.drawPixel(j + position, 0, Color.toIntBits(255, blu, gre, red));
                }
                for (int j = 0; j < interval / 2; j++) {
                    int red = (int) (dr * (interval / 2 - j));
                    int gre = (int) (dg * (interval / 2 - j));
                    int blu = (int) (db * (interval / 2 - j));
                    p1.drawPixel(j + position + interval / 2, 0, Color.toIntBits(255, blu, gre, red));
                }
            }
            position += interval;
        }
    }


    public void zebra(Pixmap p1, int length,
                      int minInterval, int maxInterval) {

        int position = 0;
        int interval = minInterval + rnd.nextInt(maxInterval - minInterval);

        p1.setColor(0,0,0,1);
        p1.fill();

        int[] color;

        float r;
        float g;
        float b;

        do
        {
            color = scheme.getColor();

            r = color[0];
            g = color[1];
            b = color[2];
        }
        while(r == g && g == b);    // prevent grey

        boolean useColor = false;

        while (position < length)
        {
            if(useColor == true && position + interval < length) {
                do
                {
                    color = scheme.getColor();

                    r = color[0];
                    g = color[1];
                    b = color[2];
                }
                while(r == g && g == b);    // prevent grey
                for (int j = 0; j - 1 < interval; j++) {
                    int red = (int) (r);
                    int gre = (int) (g);
                    int blu = (int) (b);
                    p1.drawPixel(j + position, 0, Color.toIntBits(255, blu, gre, red));
                }
                position += interval;
                useColor = false;
            }
            else
            {
                for (int j = 0; j - 1 < interval; j++) {
                    p1.drawPixel(j + position, 0, Color.toIntBits(0,0,0,255));
                }
                position += interval;
                useColor = true;
            }
        }

    }


    public void tie(Pixmap p1, int length,
                      int minInterval, int maxInterval) {

        int position = 0;
        int interval = minInterval + rnd.nextInt(maxInterval - minInterval);

        p1.setColor(0,0,0,1);
        p1.fill();

        int[] color;

        float r;
        float g;
        float b;

        do
        {
            color = scheme.getColor();

            r = color[0];
            g = color[1];
            b = color[2];
        }
        while(r == g && g == b);    // prevent grey

        boolean useColor = false;

        while (position < length)
        {
            if(useColor == true && position + interval < length) {
                do
                {
                    color = scheme.getColor();
                    r = color[0];
                    g = color[1];
                    b = color[2];
                }
                while(r == g && g == b);    // prevent grey
                for (int j = 0; j - 1 < interval; j++) {
                    int red = (int) (r);
                    int gre = (int) (g);
                    int blu = (int) (b);
                    p1.drawPixel(j + position, 0, Color.toIntBits(255, blu, gre, red));
                }
                position += interval;
                useColor = false;
            }
            else
            {
                int intervalWhite = minInterval / 3;
                for (int j = 0; j - 1 < intervalWhite; j++) {
                    p1.drawPixel(j + position, 0, Color.toIntBits(255, 255, 255, 255));
                }
                position += intervalWhite;
                useColor = true;
            }
        }

    }

    public void sine(Pixmap p1, int length, int minInterval, int maxInterval) {
        int position = 0;

        p1.setColor(0, 0, 0, 1);
        p1.fill();

        float lum;


        int frequencyLow    = 4 + 2 * rnd.nextInt(8);
        int frequencyMedium = frequencyLow  * 2 * (1 + rnd.nextInt(4));
        int frequencyHigh = frequencyMedium * 2 * (1 + rnd.nextInt(4));

        System.out.println("aaa MI: " + maxInterval + ", frequencyLow: " + frequencyLow + ", frequencyMedium: " + frequencyMedium + ", frequencyHigh: " + frequencyHigh);

        ArrayList<int[]> colors = new ArrayList<int[]>();

        // new
        int numColors = frequencyLow;

        for(int i = 0; i < numColors; i++) {
            colors.add(scheme.getColor());
        }

        while(position < length) {

            int index = (int)((position / (float)length) * numColors);
            int[] colorInt = colors.get(index);
            float[] colorFloat = new float[] { colorInt[0] / 256f, colorInt[1] / 256f, colorInt[2] / 256f };
            float[] colorHsl = com.beachcafesoftware.kensholite.Color.rgbToHsl(colorFloat[0],colorFloat[1],colorFloat[2]);

            lum =
                    0.249f +
                    (float)Math.sin(frequencyLow    * (position / (float)length) * (2 * Math.PI) + (Math.PI * 1.5)) * 0.05f +
                    (float)Math.sin(frequencyMedium * (position / (float)length) * (2 * Math.PI) + (Math.PI * 1.5)) * 0.07f +
                    (float)Math.sin(frequencyHigh   * (position / (float)length) * (2 * Math.PI) + (Math.PI * 1.5)) * 0.13f +
                   0;
            colorHsl[2] = lum;

            float[] color = com.beachcafesoftware.kensholite.Color.hslToRgb(colorHsl[0],colorHsl[1],colorHsl[2]);

            int r = (int)(Math.max(color[0],0.01) * 256);
            int g = (int)(Math.max(color[1],0.01) * 256);
            int b = (int)(Math.max(color[2],0.01) * 256);

            p1.drawPixel(position, 0, Color.toIntBits(255, b, g, r));
            position ++;
        }
    }


    public void outlined(Pixmap p1, int length,
                               int minInterval, int maxInterval)
    {
        int position = 0;

        p1.setColor(0,0,0,1);
        p1.fill();

        int thickInterval = maxInterval;
        int thinInterval = minInterval;
        boolean thin = true;

        while(position < length) {
            int[] color;
            int interval = thin == true ? thinInterval : thickInterval;

            if(interval + position >= length)
            {
                interval = length - position ;
            }

            if(rnd.nextFloat() < 0.9) {
                color = scheme.getColor();

                float dr = color[0] / (interval / 2f);
                float dg = color[1] / (interval / 2f);
                float db = color[2] / (interval / 2f);

                for (int j = 0; j - 1 < interval / 2; j++) {
                    int red = (int) (dr * j);
                    int gre = (int) (dg * j);
                    int blu = (int) (db * j);
                    p1.drawPixel(j + position, 0, Color.toIntBits(255, blu, gre, red));
                }
                for (int j = 0; j < interval / 2; j++) {
                    int red = (int) (dr * (interval / 2 - j));
                    int gre = (int) (dg * (interval / 2 - j));
                    int blu = (int) (db * (interval / 2 - j));
                    p1.drawPixel(j + position + interval / 2, 0, Color.toIntBits(255, blu, gre, red));
                }
            }
            position += interval;
            thin = !thin;
        }
    }


    public void blinds(Pixmap p1, int length,
                      int minInterval, int maxInterval) {

        p1.setColor(0,0,0,1);
        p1.fill();

        int position = 0;
        int interval = minInterval + rnd.nextInt(maxInterval - minInterval);

        int[] color1 = scheme.getColor();
        int[] color2 = scheme.getColor();
        int[] firstColor = color1;

        while (position < length) {
            if(interval + position >= length) { interval = length - position; }

            int numInnerIntervals = 10;
            int innerInterval = interval / numInnerIntervals;
            color1 = scheme.getColor();

//            System.out.println("i");
            int color1Thickness = 0;

            int distanceTravelled = 0;
            for(int i = 0; i < numInnerIntervals; i++) {
                distanceTravelled +=  innerInterval;

                int beginTwoColors = position + i * innerInterval;
//                System.out.println("j");

                for(int j = 0; j < innerInterval; j++) {
                    if(j < color1Thickness) {
                        int red = color1[0];
                        int gre = color1[1];
                        int blu = color1[2];
                        p1.drawPixel(j + beginTwoColors, 0, Color.toIntBits(255, blu, gre, red));
//                        System.out.println("c1");
                    }
                    else {
                        int red = color2[0];
                        int gre = color2[1];
                        int blu = color2[2];
                        p1.drawPixel(j + beginTwoColors, 0, Color.toIntBits(255, blu, gre, red));
//                        System.out.println("c2");
                    }
                }
                color1Thickness += innerInterval / numInnerIntervals;
            }

//            position += interval;
            position += distanceTravelled;
            if(length - position < interval) {
                position = length ;
            }
            color2 = color1;
        }

    }

    public ColorTable(ColorScheme scheme) {
        this.scheme = scheme;
    }

}
