package com.beachcafesoftware.kensholite;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

/**
 * Created by ubuntu on 21/04/18.
 */


// TODO: give each shader type its own header file. this is because there are currently redundant variables in the headers, eg this pools shader header file contains variables for rotation and numpetals.

public class PoolsRenderer extends KenshoPointBasedRenderer {

    @Override
    public String getShader() {
        String shader = "";

        shader += getHeader();
        shader += getMain();
        shader += getHeightFunction();
        shader += getHeightFunctionSine();

//        System.out.println(shader);
        return shader;
    }


    public PoolsRenderer(int numSpouts, int numSinks, boolean quantumEffect, boolean grainEffect, boolean hueShiftEffect, boolean fuzzyEdgesEffect) {
        super(numSpouts, numSinks, quantumEffect, grainEffect, hueShiftEffect, fuzzyEdgesEffect);
    }


    private String getMain() {
        String main =
                "void main() {\n" +
                        "highp float lum = 0.5;\n" +
                        "highp float totaladd = 0.0;\n";

        for(int i = 0; i < numSpouts; i++) {
            main += "totaladd = totaladd + heightFunction(vec2(spoutsposx[" + i + "], spoutsposy[" + i + "])) * spoutssize[" + i + "];\n";
        }

        main +=
                "lum = lum + totaladd * 0.0033;\n" +
                "highp float totalminus = 0.0;\n";

        for(int i = 0; i < numSinks; i++) {
            main += "totalminus = totalminus + heightFunction(vec2(sinksposx[" + i + "], sinksposy[" + i + "])) * sinkssize[" + i + "];\n";
        }

        main += "lum = lum - totalminus * 0.0033;\n";

        if(quantumEffect == true || grainEffect == true || fuzzyEdgesEffect == true || hueShiftEffect == true) {
            main += getNoiseValue();
        }

        if(fuzzyEdgesEffect == true) {
            main += getFuzzyEdges();
        }

        main +=
                "// maybe insert some sin noise if the user wants it\n" +
                "//    lum = lum + (sin(v_texCoords[0] * (freq1 / longside)) + sin(v_texCoords[1] * (freq2 / longside))) * strength;\n" +
                "clamp(lum, 0.0, 1.0);\n" +

                "lum = lum + (float(cyclePosition) / STEPS); // was 4096.0\n" +
                "lum = lum - floor(lum);\n" +

                "vec2 texCoords = vec2(lum,0.5);\n" +
                "gl_FragColor = texture2D(tex1, texCoords);\n";


        if(quantumEffect == true) {
            main += getQuantum();
        }

        if(grainEffect == true) {
            main += getGrain();
        }

        if(hueShiftEffect == true) {
            main += getHueShift();
        }


        main +=
                "}\n";

        return main;
    }



    private String getHeader() {
        String header;
        String filepath = "pools_header.txt";
        FileHandle handle = Gdx.files.internal(filepath);
        header = handle.readString();
        return header;
    }

    private String getHeightFunction() {
        String heightFunction;

        heightFunction =
                "lowp float heightFunction(vec2 position)\n" +
                "{\n" +
                "lowp float dx = (position[0] - v_texCoords[0]);\n" +
                "lowp float dy = (position[1] - v_texCoords[1]) * landscape_aspect_ratio;\n" +
                "return inversesqrt(dx * dx + dy * dy);\n" +
                "}\n";

        return heightFunction;
    }

}




