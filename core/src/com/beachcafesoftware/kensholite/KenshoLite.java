package com.beachcafesoftware.kensholite;

import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.TimeUtils;


public class KenshoLite extends ApplicationAdapter  {

	private static final float FADE_DURATION_SECONDS = 1.0f;
	private static final float ONE_OVER_THIRTY = 1f / 30f;
	private static final String IP = "8.8.8.8";
	public static boolean prefsChanged;

	private static final String[] SHADERS = new String[] { "normal","flower","waves","lightning","spiral","rays","wedge" };
	private static final String[] RENDER_SCHEMES = new String[] { "halfsmooth","rampluminance","zebra","tie","outlined","blinds","sine" };
	private static final String[] COLOR_SCHEMES = new String[] { "anycolor", "tripod", "desert", "forest" };

	private final float[] cycleSpeeds = new float[] {    0,	 	0.33f,    0.66f,   1.32f,   2.64f, 	5.28f	 };
	private final float[] movementSpeeds = new float[] { 0,		0.00025f,0.0005f,0.001f, 0.002f, 0.004f };
	private final float[] freqs = new float[] {			10000, 20000, 40000, 80000, 160000, 320000		  };
	private final float[] strengths = new float[]{		 0, 	0.0005f,	0.001f,	0.002f,	0.004f,	0.008f,	0.016f    };
	private final int MAX_POINTS = 8;
	private int numberOfPoints;		// this changes every randomisation
	private SpriteBatch sb;
	private ShaderProgram shaderFirst;
	private ShaderProgram shaderSecond;
	private int currentFrame = 0;
	private String vertexShaderString;
	private Texture t1;
	private Random rnd;
 	private float[] points = new float[4 * 5];
	private PointKensho[] pointsKensho;
	private float[] pointsKenshoFloat = new float[MAX_POINTS * 2];	// because y is stored after each x
	private PointKensho[] pointsKenshoSink;
	private float[] pointsKenshoSinkFloat = new float[MAX_POINTS * 2]; // same maximum number for sinks, just because
	private Texture textureColorTable;
	private Pixmap p1;
	private double colorCycleRate;
	private double colorCyclePosition;
	private int STEPS = 2048; // was 4096
	private Preferences prefs;
	private float movementSpeed;
	private int numInwardKnots;
	private int numOutwardKnots;
	private String renderScheme;
	private String colorScheme;
	private float distFreq1;
	private float distFreq2 = 60000;
	private float distStrength;
	private int numPetals;
	private double addAngle;
	private double raysAngle;
	private ColorScheme scheme;
	private String shaderType1;
	private String shaderType2;
	private FPSLogger fps1 = new FPSLogger();
	private long runningTime;
	private long lastSwitch;
	private float addAngleDelta;
	private int numArms;

 	private FrameBuffer fbLayer1;
	private FrameBuffer fbLayer2;
	private String imageQuality;
	private String filtering;
	private int targetFps;
	private boolean drawSecondLayer;
	private int numFields;
	private long sleepDuration = 10;

	private boolean quantumEffect = false;
	private boolean grainEffect = false;
	private boolean hueShiftEffect = false;
	private boolean fuzzyEdgesEffect = false;

	private boolean blending = false; 	// false: subtractive, true: additive
	private float brightness = 0.0f;

	private static float alphaLayer1 = 0.0f;
	private static float alphaLayer2 = 0.0f;

	static private int MAX_SPOUTS = 8;

	final static private float[] spoutsX = new float[MAX_SPOUTS];
	final static private float[] sinksX = new float[MAX_SPOUTS];
	final static private float[] spoutsY = new float[MAX_SPOUTS];
	final static private float[] sinksY = new float[MAX_SPOUTS];
	final static private float[] spoutsSize = new float[MAX_SPOUTS];
	final static private float[] sinksSize = new float[MAX_SPOUTS];

	private GdxAppUsageLogger appLogger;

	private FrameBuffer fbLayer1FadeOut;
	private FrameBuffer fbLayer2FadeOut;

	private boolean thirtyFps = false;

	private Map<String, ShaderProgram> shaderStore;
	private ShaderProgram shader1Queued;
	private ShaderProgram shader2Queued;


	@Override
	public void create () {

		shaderStore = new HashMap<String, ShaderProgram>();

 		rnd = new Random();
		vertexShaderString = Gdx.files.internal("vertex.glsl").readString();

		sb = new SpriteBatch();

		Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGB565);
		pixmap.setColor(1,1,1,1);
		pixmap.fill();

		t1 = new Texture(pixmap);
		t1.draw(pixmap, 0, 0);
		pixmap.dispose();

		shaderFirst.pedantic = false;

		IntBuffer buf = BufferUtils.newIntBuffer(16);
		Gdx.gl.glGetIntegerv(GL20.GL_MAX_TEXTURE_SIZE, buf);
		int maxSize = buf.get(0);
		STEPS = maxSize;

		randomise();

		System.out.println("iscompiled: " + shaderFirst.isCompiled() + " : " + shaderFirst.getLog());

		appLogger = new KenshoLogger();
		appLogger.setInterval(60 * 60 * 6);
		appLogger.setApplicationName("kensholite");
		appLogger.setUrl("http://" + IP + "/logappusage.php");

		try {
			appLogger.log();
		} catch (ParametersNotSetException e) {
			e.printStackTrace();
		}
	}


	private void sanitisePreferences() {
		prefs =  Gdx.app.getPreferences("cube2settings");

		String runTimeS = prefs.getString("runningtime");
		String cycleSpeedsS = prefs.getString("cyclespeed");
		String movementSpeedsS = prefs.getString("movementspeed");
		String minKnotsS = prefs.getString("minknots");
		String maxKnotsS = prefs.getString("maxknots");
		String renderSchemeS = prefs.getString("renderscheme");
		String colorSchemeS = prefs.getString("colorscheme");
		String shader1S = prefs.getString("shader1");
		String shader2S = prefs.getString("shader2");
		String distFreq1S = prefs.getString("distortionfrequency1");
		String distFreq2S = prefs.getString("distortionfrequency2");
		String distStrengthS = prefs.getString("distortionstrength");
		String imageQualityS = prefs.getString("imagequality");
		String targetFpsS = prefs.getString("targetfps");
		String filteringS = prefs.getString("filtering");
		String blendingS = prefs.getString("blending");
		String quantumS = prefs.getString("quantum");
		String grainS = prefs.getString("grain");
		String hueShiftS = prefs.getString("hueshift");
		String fuzzyEdgesS = prefs.getString("fuzzyedges");
		String brightnessS = prefs.getString("brightness");

		// defaults if empty
		if(runTimeS.equals(""))			{ prefs.putString("runningtime","10"); }
		if(cycleSpeedsS.equals("")) 	{ prefs.putString("cyclespeed","random"); }
		if(movementSpeedsS.equals("")) 	{ prefs.putString("movementspeed","1"); }
		if(minKnotsS.equals("")) 		{ prefs.putString("minknots","1"); }
		if(maxKnotsS.equals(""))		{ prefs.putString("maxknots","5"); }
		if(renderSchemeS.equals(""))	{ prefs.putString("renderscheme","random"); }
		if(colorSchemeS.equals(""))		{ prefs.putString("colorscheme","random1"); } if(colorSchemeS.equals("random")){ prefs.putString("colorscheme","random1"); }
		if(shader1S.equals(""))			{ prefs.putString("shader1","random"); }
		if(shader2S.equals(""))			{ prefs.putString("shader2","random"); }
		if(distFreq1S.equals(""))		{ prefs.putString("distortionfrequency1","random"); }
		if(distFreq2S.equals(""))		{ prefs.putString("distortionfrequency2","random"); }
		if(distStrengthS.equals(""))	{ prefs.putString("distortionstrength","0"); }
		if(imageQualityS.equals(""))	{ prefs.putString("imagequality","medium"); }
		if(filteringS.equals(""))		{ prefs.putString("filtering","nearest"); }
		if(targetFpsS.equals(""))		{ prefs.putString("targetfps","30"); }
		if(blendingS.equals(""))		{ prefs.putString("blending","random"); }
		if(quantumS.equals(""))			{ prefs.putString("quantum","off"); }
		if(grainS.equals(""))			{ prefs.putString("grain","off"); }
		if(hueShiftS.equals(""))		{ prefs.putString("hueshift","random"); }
		if(fuzzyEdgesS.equals(""))		{ prefs.putString("fuzzyedges","off"); }
		if(brightnessS.equals(""))		{ prefs.putString("brightness","80"); }

		prefs.flush();
	}

	private void loadPreferences() {

		this.runningTime = Integer.parseInt(prefs.getString("runningtime"));
		this.targetFps = Integer.parseInt(prefs.getString("targetfps"));
		this.filtering = prefs.getString("filtering");
		this.brightness = Integer.parseInt(prefs.getString("brightness")) * 0.01f;


		if(prefs.getString("cyclespeed").equals("random") == false) { this.colorCycleRate = cycleSpeeds[Integer.parseInt(prefs.getString("cyclespeed"))]; }
		else {
			if(
					prefs.getString("movementspeed").equals("random") == true ||
							prefs.getString("movementspeed").equals("0") == true
					) {
				this.colorCycleRate = cycleSpeeds[1 + rnd.nextInt(4)];
			}
			else {
				this.colorCycleRate = cycleSpeeds[rnd.nextInt(5)];
			}
		}

		if(prefs.getString("movementspeed").equals("random") == false) { this.movementSpeed = movementSpeeds[Integer.parseInt(prefs.getString("movementspeed"))]; }
		else {
			if(
					prefs.getString("cyclespeed").equals("random") == true ||
							prefs.getString("cyclespeed").equals("0") == true
					) {
				this.movementSpeed = movementSpeeds[1 + rnd.nextInt(4)];
			}
			else {
				this.movementSpeed = movementSpeeds[rnd.nextInt(5)];
			}
		}

		int minimumKnots = Integer.parseInt(prefs.getString("minknots"));
		int maximumKnots = Math.max(Integer.parseInt(prefs.getString("maxknots")), minimumKnots);
		int numberOfKnots = minimumKnots + rnd.nextInt(1 + maximumKnots - minimumKnots);

		int numberOfInwardKnots = rnd.nextInt(numberOfKnots);
		int numberOfOutwardKnots = numberOfKnots - numberOfInwardKnots;

		this.numOutwardKnots = numberOfOutwardKnots;
		this.numInwardKnots = numberOfInwardKnots;

		if(prefs.getString("renderscheme").equals("random") == false) {	this.renderScheme = prefs.getString("renderscheme"); }
		else { this.renderScheme = RENDER_SCHEMES[rnd.nextInt(RENDER_SCHEMES.length)]; }

		if(prefs.getString("colorscheme").equals("random1") == false) { 	this.colorScheme = prefs.getString("colorscheme"); }
		else { this.colorScheme = COLOR_SCHEMES[rnd.nextInt(COLOR_SCHEMES.length)]; }

		if(prefs.getString("distortionfrequency1").equals("random") == false) { this.distFreq1 = freqs[Integer.parseInt(prefs.getString("distortionfrequency1")) - 1]; }
		else { this.distFreq1 = freqs[rnd.nextInt(6)]; }

		if(prefs.getString("distortionstrength").equals("random") == false) { this.distStrength = strengths[Integer.parseInt(prefs.getString("distortionstrength"))]; }
		else { this.distStrength = strengths[rnd.nextInt(7)]; }

		if(prefs.getString("blending").equals("random") == false) { this.blending = prefs.getString("blending").equals("additive"); }
		else { this.blending = rnd.nextBoolean(); }

		if(prefs.getString("quantum").equals("random") == false) { this.quantumEffect = prefs.getString("quantum").equals("on"); }
		else { this.quantumEffect = rnd.nextBoolean(); }

		if(prefs.getString("grain").equals("random") == false) { this.grainEffect = prefs.getString("grain").equals("on"); }
		else { this.grainEffect = rnd.nextBoolean(); }

		if(prefs.getString("hueshift").equals("random") == false) { this.hueShiftEffect = prefs.getString("hueshift").equals("on"); }
		else { this.hueShiftEffect = rnd.nextBoolean(); }

		if(prefs.getString("fuzzyedges").equals("random") == false) { this.fuzzyEdgesEffect = prefs.getString("fuzzyedges").equals("on"); }
		else { this.fuzzyEdgesEffect = rnd.nextBoolean(); }

		if(prefs.getString("shader1").equals("random") == false) { this.shaderType1 = prefs.getString("shader1"); }
		else {
			 this.shaderType1 = SHADERS[rnd.nextInt(SHADERS.length)];
		}

		if(prefs.getString("shader2").equals("random") == false) { this.shaderType2 = prefs.getString("shader2"); }
		else {
			shaderType2 = shaderType1;

			while(shaderType2.equals(shaderType1)) {
				this.shaderType2 = SHADERS[rnd.nextInt(SHADERS.length)];
			}
		}

		this.imageQuality = prefs.getString("imagequality");
	}


	// TODO: Queue up the next scene's variable bundle and shader in another thread,
	// TODO: and just take a variable bundle and shader off the queue on
	// TODO: this thread. This will stop the minor fps drop between scenes.
	public void randomise() {

		sanitisePreferences();
		loadPreferences();

		// set some fields based on the results of loadPreferences, but which are not the fields directly set by the prefs
		alphaLayer1 = blending == false ? 1.0f : 0.5f;
		alphaLayer2 = blending == false ? 0.5f : 0.5f;
		targetFps = 30;
		thirtyFps = targetFps == 30;
		fuzzyEdgesEffect = false;

		/*
		// when the variables and shader queueing is ready,
		// prefsChanged is needed to decide whether we can take a queued up variable
		// bundle and shader, or whether we need to create new ones according to the new preferences.
		// we can set prefsChanged back to false, if it is true, after creating the new bundle and shader.

		shader1Queued = null;
		shader2Queued = null;
		*/
		prefsChanged = false;

		currentFrame = 0;

		if(fbLayer1FadeOut != null) {
 			fbLayer1FadeOut.dispose();
		}
		if(fbLayer2FadeOut != null) {
 			fbLayer2FadeOut.dispose();
		}

		if(fbLayer1 != null && fbLayer1.getColorBufferTexture() != null) {
			fbLayer1FadeOut = fbLayer1;
		}

		if(fbLayer2 != null && fbLayer2.getColorBufferTexture() != null) {
			fbLayer2FadeOut = fbLayer2;
		}

		if(numInwardKnots == 0 && numOutwardKnots == 0) {
			distStrength = strengths[5 + rnd.nextInt(2)];
		}

 		colorCyclePosition = 0;
		colorCycleRate = ((colorCycleRate * 0.5f) + (colorCycleRate * rnd.nextDouble() * 0.5f)) * (60.0f / targetFps);

		colorCycleRate *= STEPS / 4096d;

		numberOfPoints = Math.max(this.numInwardKnots,this.numOutwardKnots);

		distFreq1 =  (distFreq1 * 0.5f) + (distFreq1 * 0.5f * rnd.nextFloat());
		distFreq2 =  (distFreq2 * 0.5f) + (distFreq2 * 0.5f * rnd.nextFloat());


		distStrength = (distStrength * 0.5f) + (distStrength * 0.5f * rnd.nextFloat());

		numPetals = 4 + 2 * rnd.nextInt(8);
		numFields = 2 + 2 * rnd.nextInt(8);
		numArms   = 2 + 2 * rnd.nextInt(8);

		addAngle = 0;
		addAngleDelta = (-0.01f + rnd.nextFloat() * 0.02f) * (60.0f / targetFps);
		raysAngle = rnd.nextFloat() * 2 * Math.PI;

		p1 = new Pixmap(STEPS,1, Pixmap.Format.RGB888);

		     if(colorScheme.equals("anycolor"))	{ scheme = new RandomRGBColorScheme(); }
		else if(colorScheme.equals("tripod"))	{ scheme = new TripodColorScheme(); }
		else if(colorScheme.equals("desert"))		{ scheme = new DesertColorScheme(); }
		else if(colorScheme.equals("forest"))		{ scheme = new ForestColorScheme(); }

		float mult = 1;

		     if(renderScheme.equals("halfsmooth")) {	new ColorTable(scheme).halfSmooth(				p1, STEPS,(int)(24 * mult * (STEPS / 4096d)), (int)(72 * mult * (STEPS / 4096d))); }
		else if(renderScheme.equals("rampluminance")) {	new ColorTable(scheme).rampLuminance(			p1, STEPS,(int)(24 * mult * (STEPS / 4096d)), (int)(72  * mult * (STEPS / 4096d)));			}
		else if(renderScheme.equals("zebra")) {			new ColorTable(scheme).zebra(					p1, STEPS,(int)(24 * mult * (STEPS / 4096d)), (int)(48  * mult * (STEPS / 4096d)));			}
		else if(renderScheme.equals("tie")) {			new ColorTable(scheme).tie(						p1, STEPS,(int)(24 * mult * (STEPS / 4096d)), (int)(72  * mult * (STEPS / 4096d)));			}
		else if(renderScheme.equals("outlined")) {		new ColorTable(scheme).outlined(				p1, STEPS,(int)(32 * mult * (STEPS / 4096d)) , (int)(96  * mult * (STEPS / 4096d)));			}
		else if(renderScheme.equals("blinds")) {		new ColorTable(scheme).blinds(					p1, STEPS,(int)(1  * mult * (STEPS / 16 - 1)), (int)(1  * mult * (STEPS / 16))); }
		else if(renderScheme.equals("sine")) {			new ColorTable(scheme).sine(					p1, STEPS,(int)(1  * mult * (STEPS / 64 - 1)), (int)(1  * mult * (STEPS / 64))); }

		if(textureColorTable != null) { textureColorTable.dispose(); }

		textureColorTable = new Texture(p1);
		p1.dispose();


		if(pointsKensho == null) {
			pointsKensho = new PointKensho[MAX_POINTS];
		}
		if(pointsKenshoSink == null) {
			pointsKenshoSink = new PointKensho[MAX_POINTS];
		}

 		for(int i = 0; i < this.MAX_POINTS; i++) {
			float movementSpeedV = ((movementSpeed * 0.5f) + (movementSpeed * rnd.nextFloat() * 0.5f)) * (60.0f / targetFps);
			float size;
			float sizes;

			if(i != 0) {
				size = 0.05f + rnd.nextFloat() * 0.95f;
			}
			else {
 				size = 1.0f;
			}
			if(i != 0) {
				sizes = 0.05f + rnd.nextFloat() * 0.95f;
			}
			else {
 				sizes = 1.0f;
			}

			if (i < numOutwardKnots) {
				pointsKensho[i] = new PointKensho(rnd.nextFloat(), rnd.nextFloat(), size, movementSpeedV, false, targetFps);
//				pointsKensho[i].calculateFrameTime(targetFps);
			}
			else {
				pointsKensho[i] = new PointKensho(-1.0f, -1.0f, true, targetFps);
			}
			if (i < numInwardKnots) {
				pointsKenshoSink[i] = new PointKensho(rnd.nextFloat(), rnd.nextFloat(), sizes, movementSpeedV, false, targetFps);
//				pointsKenshoSink[i].calculateFrameTime(targetFps);
			}
			else {
 				pointsKenshoSink[i] = new PointKensho(-1.0f, -1.0f, true, targetFps);
			}
 		}

//		if(shaderFirst != null) { shaderFirst.dispose(); shaderFirst = null; }
//		if(shaderSecond != null) { shaderSecond.dispose(); shaderSecond = null; }

		drawSecondLayer = shaderType2.equals("none") == false;

		shaderFirst = getNextShader(shaderType1);


		if(drawSecondLayer == true) {
			shaderSecond = getNextShader(shaderType2);
		}

		this.lastSwitch = TimeUtils.millis();

		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();

		if(imageQuality.equals("high")) {
			fbLayer1 = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, false);
			if (shaderType2 != null) {
				fbLayer2 = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, false);
			}
		}
		else if(imageQuality.equals("medium")){
			fbLayer1 = new FrameBuffer(Pixmap.Format.RGBA8888, width / 2, height / 2, false);
			if (shaderType2 != null) {
				fbLayer2 = new FrameBuffer(Pixmap.Format.RGBA8888, width / 2, height / 2, false);
			}
		}
		else if(imageQuality.equals("low")){
			fbLayer1 = new FrameBuffer(Pixmap.Format.RGBA8888, width / 3, height / 3, false);
			if (shaderType2 != null) {
				fbLayer2 = new FrameBuffer(Pixmap.Format.RGBA8888, width / 3, height / 3, false);
			}
		}
		else if(imageQuality.equals("verylow")){
			fbLayer1 = new FrameBuffer(Pixmap.Format.RGBA8888, width / 4, height / 4, false);
			if (shaderType2 != null) {
				fbLayer2 = new FrameBuffer(Pixmap.Format.RGBA8888, width / 4, height / 4, false);
			}
		}

		Texture.TextureFilter filter = filtering.equals("nearest") ? Texture.TextureFilter.Nearest : Texture.TextureFilter.Linear;
		fbLayer1.getColorBufferTexture().setFilter(filter, filter);
		fbLayer2.getColorBufferTexture().setFilter(filter, filter);

		System.out.println("shaderFirst  iscompiled: " + shaderFirst.isCompiled() + " : " + shaderFirst.getLog());

		if(shaderSecond != null) {
			System.out.println("shaderSecond iscompiled: " + shaderSecond.isCompiled() + " : " + shaderSecond.getLog());
		}
	}

	private int bToI(boolean bool) {
		return bool == true ? 1 : 0;
	}

	// TODO: Although this is mostly working well, some things are presenting a problem.
	// TODO: Primarily the variables that are randomly generated in the Renderer constructor.
	// TODO: For example, spiralFactor in SpiralRenderer.
	// TODO: Perhaps these need to be moved out of the implementation and brought back to the
	// TODO: main class so that they can be passed as shader parameters each frame.
	// TODO: Perhaps each renderer can load from the shaderstore, as it knows about its own
	// TODO: parameters and can create its own keys with its own length. - probably offers no benefit
	// TODO: because it will create a huge number of shaders, one for each value of, eg, spiralFactor,
	// TODO: and almost never reuse them.
	private ShaderProgram getNextShader(String shaderType) {

		ShaderProgram shaderProgram = null;

		String shaderString;
		String shaderKey = shaderType + "." + numOutwardKnots + "." + numInwardKnots + "." + bToI(quantumEffect) + "." + bToI(grainEffect) + "." + bToI(hueShiftEffect)+ "." + bToI(fuzzyEdgesEffect);

		if(shaderStore.containsKey(shaderKey)) {
			shaderProgram = shaderStore.get(shaderKey);
			System.out.println("aaa " + shaderKey + " got from store");
		}
		else {
			if (shaderType.equals("normal")) {
				shaderString = (new PoolsRenderer(numOutwardKnots, numInwardKnots, quantumEffect, grainEffect, hueShiftEffect, fuzzyEdgesEffect)).getShader();
			} else if (shaderType.equals("waves")) {
				shaderString = (new WavesRenderer(numOutwardKnots, numInwardKnots, quantumEffect, grainEffect, hueShiftEffect, fuzzyEdgesEffect)).getShader();
			} else if (shaderType.equals("flower")) {
				shaderString = (new FlowerRenderer(numOutwardKnots, numInwardKnots, quantumEffect, grainEffect, hueShiftEffect, fuzzyEdgesEffect)).getShader();
			} else if (shaderType.equals("lightning")) {
				shaderString = (new FieldsRenderer(numOutwardKnots, numInwardKnots, quantumEffect, grainEffect, hueShiftEffect, fuzzyEdgesEffect)).getShader();
			} else if (shaderType.equals("spiral")) {
				shaderString = (new SpiralRenderer(numOutwardKnots, numInwardKnots, quantumEffect, grainEffect, hueShiftEffect, fuzzyEdgesEffect)).getShader();
			} else if (shaderType.equals("rays")) {
				shaderString = (new RaysRenderer(numOutwardKnots, numInwardKnots, quantumEffect, grainEffect, hueShiftEffect, fuzzyEdgesEffect)).getShader();
			} else if (shaderType.equals("wedge")) {
				shaderString = (new WedgeRenderer(numOutwardKnots, numInwardKnots, quantumEffect, grainEffect, hueShiftEffect, fuzzyEdgesEffect)).getShader();
			} else if (shaderType.equals("none")) {
				shaderString = null;
			} else {
				System.out.println("unknown shader selected in prefs");
				shaderString = null;
			}
			System.out.println("aaa " + shaderKey + " putting in store");

			shaderProgram = new ShaderProgram(vertexShaderString, shaderString);
			shaderStore.put(shaderKey, shaderProgram);
		}

		return shaderProgram;
	}


	@Override
	public void render() {

		if(thirtyFps) {
			try {
				Thread.sleep(sleepDuration);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(Gdx.graphics.getDeltaTime() > ONE_OVER_THIRTY) {
				sleepDuration --;
				if(sleepDuration < 0) sleepDuration = 0;
			}
			else {
				sleepDuration ++;
			}
		}

		if (TimeUtils.millis() > this.lastSwitch + this.runningTime * 1000 || KenshoLite.prefsChanged == true) {
			randomise();
			//	currentFrame = 0;
		}

		/*
		if (KenshoLite.prefsChanged == true) {
			prefsChanged = false;
			shader1Queued = null;
			shader2Queued = null;
		}
		*/

		Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();

		for (int i = 0; i < numberOfPoints; i++) {
			pointsKensho[i].updatePosition();
			pointsKenshoFloat[i * 2] = pointsKensho[i].x;
			pointsKenshoFloat[i * 2 + 1] = pointsKensho[i].y;

			pointsKenshoSink[i].updatePosition();
			pointsKenshoSinkFloat[i * 2] = pointsKenshoSink[i].x;
			pointsKenshoSinkFloat[i * 2 + 1] = pointsKenshoSink[i].y;
		}

		shaderFirst.setAttributef("heightv", (float) height, 0f, 0f, 0f);
		shaderFirst.setAttributef("widthv", (float) width, 0f, 0f, 0f);

		// maybe change this to be called on oncreate, onresume, onresize, etc
		if ((currentFrame % (targetFps * 2)) == 0) {
			sb.dispose();
			sb = new SpriteBatch();
		}

		// fade-out with time, not frames
		long timeCurrent = TimeUtils.millis();
		long fadeTimeEnd = lastSwitch + ((int)(FADE_DURATION_SECONDS * 1000));
		float fadeProgress = (timeCurrent - lastSwitch) / (float)(fadeTimeEnd - lastSwitch);

		if(timeCurrent < fadeTimeEnd) {

			// TODO: set gl blending type...
			// TODO: don't know why GL additive shouldn't be used as it is later on... but everything goes white.
			// TODO: possibly because now we're adding 4 layers all together.
			//			if(blending == false) { sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA); } // subtractive
			//		else { sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);	} // additive

			sb.begin();
			sb.setShader(null);

			float fadeAlpha;

			// layer 1
			fadeAlpha = alphaLayer1 - (fadeProgress * alphaLayer1);

			if(fbLayer1FadeOut != null) {
				sb.setColor(brightness, brightness, brightness, fadeAlpha);
				sb.draw(fbLayer1FadeOut.getColorBufferTexture(), 0f, 0f, width, height);
			}

			// layer 2
			fadeAlpha = alphaLayer2 - (fadeProgress * alphaLayer2);

			if(fbLayer2FadeOut != null) {
				sb.setColor(brightness, brightness, brightness, fadeAlpha);
				sb.draw(fbLayer2FadeOut.getColorBufferTexture(), 0f, 0f, width, height);
			}

			sb.end();
		}


		// first layer
		fbLayer1.begin();
		sb.begin();
		sb.setShader(shaderFirst);

		setUniforms(shaderFirst);

		// textureColorTable. both the kensho textureColorTable and a default blank one need to be binded, like this, and in reverse order.

		textureColorTable.bind(1);
		shaderFirst.setUniformi("tex1", 1);
		t1.bind(0);
		shaderFirst.setUniformi("u_texture", 0);

		// color cycle position
		shaderFirst.setUniformi("cyclePosition", (int) colorCyclePosition);

		// number of spouts
		shaderFirst.setUniformi("numSpouts", this.numOutwardKnots);

		// number of sinks
		shaderFirst.setUniformi("numSinks", this.numInwardKnots);

		// distortion grid parameters
		shaderFirst.setUniformf("freq1", this.distFreq1);
		shaderFirst.setUniformf("freq2", this.distFreq2);
		shaderFirst.setUniformf("strength", this.distStrength);

		// petals
		shaderFirst.setUniformf("numPetals", (float) this.numPetals);

		// fields
		shaderFirst.setUniformf("numFields", (float) this.numFields);

		// arms (spirals)
		shaderFirst.setUniformf("numArms", (float) this.numArms);

		// rotation angle
		shaderFirst.setUniformf("addAngle", (float) addAngle);

		// rays angle (non-rotating)
		shaderFirst.setUniformf("raysAngle", (float) raysAngle);

		// long side
		shaderFirst.setUniformf("longside", Math.max(width, height));
		shaderFirst.setUniformf("invwidthf", 1.0f / width);
		shaderFirst.setUniformf("landscape_aspect_ratio", (float)height / width);

		// steps (textureColorTable size)
		shaderFirst.setUniformf("STEPS", STEPS);

		shaderFirst.setUniformf("noise_seed_phase1", currentFrame);
		shaderFirst.setUniformf("noise_seed_phase2", currentFrame + 10);
		shaderFirst.setUniformf("noise_seed_phase3", currentFrame + 20);

		shaderFirst.setUniformf("nsp1x", (float) (-0.5f + rnd.nextDouble() * 2.0f));
		shaderFirst.setUniformf("nsp1y", (float) (-0.5f + rnd.nextDouble() * 2.0f));
		shaderFirst.setUniformf("nsp2x", (float) (-0.5f + rnd.nextDouble() * 2.0f));
		shaderFirst.setUniformf("nsp2y", (float) (-0.5f + rnd.nextDouble() * 2.0f));
		shaderFirst.setUniformf("nsp3x", (float) (-0.5f + rnd.nextDouble() * 2.0f));
		shaderFirst.setUniformf("nsp3y", (float) (-0.5f + rnd.nextDouble() * 2.0f));

		float color = com.badlogic.gdx.graphics.Color.toFloatBits(255, 255, 255, 255);

		points[0] = 0;
		points[1] = 0;
		points[2] = color;
		points[3] = 0;
		points[4] = 0;

		points[5] = width;
		points[6] = 0;
		points[7] = color;
		points[8] = 1;
		points[9] = 0;

		points[10] = width;
		points[11] = height;
		points[12] = color;
		points[13] = 1;
		points[14] = 1;

		points[15] = 0;
		points[16] = height;
		points[17] = color;
		points[18] = 0;
		points[19] = 1;

		sb.draw(t1, points, 0, 20);
		sb.end();

		fbLayer1.end();
		// end layer 1

		if(drawSecondLayer == true) {
			// begin layer 2
			fbLayer2.begin();
			sb.begin();
			sb.setShader(shaderSecond);

			setUniforms(shaderSecond);

			// textureColorTable. both the kensho textureColorTable and a default blank one need to be binded, like this, and in reverse order.

			textureColorTable.bind(1);
			shaderSecond.setUniformi("tex1", 1);
			t1.bind(0);
			shaderSecond.setUniformi("u_texture", 0);

			// color cycle position
			shaderSecond.setUniformi("cyclePosition", (int) colorCyclePosition);

			// number of spouts
			shaderSecond.setUniformi("numSpouts", this.numOutwardKnots);

			// number of sinks
			shaderSecond.setUniformi("numSinks", this.numInwardKnots);

			// distortion grid parameters
			shaderSecond.setUniformf("freq1", this.distFreq1);
			shaderSecond.setUniformf("freq2", this.distFreq2);
			shaderSecond.setUniformf("strength", this.distStrength);

			// petals
			shaderSecond.setUniformf("numPetals", (float) this.numPetals);

			// fields
			shaderSecond.setUniformf("numFields", (float) this.numFields);

			// arms (spirals)
			shaderSecond.setUniformf("numArms", (float) this.numArms);

			// rotation angle
			shaderSecond.setUniformf("addAngle", (float) addAngle);

			// rays angle (non-rotating)
			shaderSecond.setUniformf("raysAngle", (float) raysAngle);

			// long side
			shaderSecond.setUniformf("longside", Math.max(width, height));
			shaderSecond.setUniformf("invwidthf", 1.0f / width);
			shaderSecond.setUniformf("landscape_aspect_ratio", (float)height / width);

			// steps (textureColorTable size)
			shaderSecond.setUniformf("STEPS", STEPS);

			shaderSecond.setUniformf("noise_seed_phase1", currentFrame);
			shaderSecond.setUniformf("noise_seed_phase2", currentFrame + 10);
			shaderSecond.setUniformf("noise_seed_phase3", currentFrame + 20);

			shaderSecond.setUniformf("nsp1x", (float) (-1.5f + rnd.nextDouble() * 4.0f));
			shaderSecond.setUniformf("nsp1y", (float) (-1.5f + rnd.nextDouble() * 4.0f));
			shaderSecond.setUniformf("nsp2x", (float) (-1.5f + rnd.nextDouble() * 4.0f));
			shaderSecond.setUniformf("nsp2y", (float) (-0.5f + rnd.nextDouble() * 2.0f));
			shaderSecond.setUniformf("nsp3x", (float) (-0.5f + rnd.nextDouble() * 2.0f));
			shaderSecond.setUniformf("nsp3y", (float) (-0.5f + rnd.nextDouble() * 2.0f));

			points[0] = 0;
			points[1] = 0;
			points[2] = color;
			points[3] = 0;
			points[4] = 0;

			points[5] = width;
			points[6] = 0;
			points[7] = color;
			points[8] = 1;
			points[9] = 0;

			points[10] = width;
			points[11] = height;
			points[12] = color;
			points[13] = 1;
			points[14] = 1;

			points[15] = 0;
			points[16] = height;
			points[17] = color;
			points[18] = 0;
			points[19] = 1;

			sb.draw(t1, points, 0, 20);
			sb.end();

			fbLayer2.end();
			// end layer 2
		}

		// draw both framebuffers
		sb.begin();
		sb.setShader(null);


		// set gl blending type
		if(blending == false) { sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA); } // subtractive
		else { sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);	} // additive

		float fadeAlpha;

		// draw first layer
		fadeAlpha = alphaLayer1;

		if(timeCurrent < fadeTimeEnd) {
			fadeAlpha = alphaLayer1 * fadeProgress;
		}

		sb.setColor(brightness, brightness, brightness, fadeAlpha);
		sb.draw(fbLayer1.getColorBufferTexture(), 0f, 0f, width, height);

		// draw second layer
		if(drawSecondLayer == true) {

			fadeAlpha = alphaLayer2;

			if(timeCurrent < fadeTimeEnd) {
				fadeAlpha = alphaLayer2 * fadeProgress;
			}

			sb.setColor(brightness, brightness, brightness, fadeAlpha);
			sb.draw(fbLayer2.getColorBufferTexture(), 0f, 0f, width, height);

		}

 		sb.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);


		// debug draw color table
		// sb.setColor(1f, 1f, 1f, 1f);
		// sb.draw(textureColorTable,0,150,width,200);

		sb.end();

		currentFrame++;
		colorCyclePosition += colorCycleRate;

		if(colorCyclePosition >= STEPS) {
			colorCyclePosition -= STEPS;
		}

		addAngle += addAngleDelta;

		if(addAngle > 2.0 * Math.PI) {
			addAngle -= 2.0 * Math.PI;
		}
		if(addAngle < -2.0 * Math.PI) {
			addAngle += 2.0 * Math.PI;
		}


 		fps1.log();
	}


	private void setUniforms(ShaderProgram shaderProgram) {

		to1dArrays(pointsKensho, pointsKenshoSink);

		// spouts
		shaderProgram.setUniform1fv("spoutsposx", spoutsX, 0, MAX_SPOUTS);
		shaderProgram.setUniform1fv("spoutsposy", spoutsY, 0, MAX_SPOUTS);
		shaderProgram.setUniform1fv("spoutssize", spoutsSize, 0, MAX_SPOUTS);

		// sinks
		shaderProgram.setUniform1fv("sinksposx", sinksX, 0, MAX_SPOUTS);
		shaderProgram.setUniform1fv("sinksposy", sinksY, 0, MAX_SPOUTS);
		shaderProgram.setUniform1fv("sinkssize", sinksSize, 0, MAX_SPOUTS);

	}

	private static void to1dArrays(PointKensho[] pointsKensho, PointKensho[] pointsKenshoSink) {

		for(int i = 0; i < pointsKensho.length; i++) {
			spoutsX[i] = pointsKensho[i].x;
			spoutsY[i] = pointsKensho[i].y;
			spoutsSize[i] = pointsKensho[i].size;
		}

		for(int i = 0; i < pointsKenshoSink.length; i++) {
			sinksX[i] = pointsKenshoSink[i].x;
			sinksY[i] = pointsKenshoSink[i].y;
			sinksSize[i] = pointsKenshoSink[i].size;
		}
	}

}
