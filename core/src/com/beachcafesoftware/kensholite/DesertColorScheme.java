package com.beachcafesoftware.kensholite;

import java.util.Random;

/**
 * Created by ubuntu on 18/12/17.
 */

public class DesertColorScheme implements ColorScheme {

    private final Random rnd;


    public DesertColorScheme() {
        rnd = new Random();
    }

    @Override
    public int[] getColor() {

        float randomValue = rnd.nextFloat();

        int r;
        int g;
        int b;

        // water
        if(randomValue < 0.025) {
            b = rnd.nextInt(256);
            g = b / 2 + rnd.nextInt(1 + b / 2);
            r = rnd.nextInt(1 + g);
        }
        // grass
        else if(randomValue < 0.1) {
            g = rnd.nextInt(256);
            b = g / 2 + rnd.nextInt(1 + g / 2);
            r = rnd.nextInt(1 + b);
        }
        // sand
        else {
            r = rnd.nextInt(256);
            g = r / 2 + rnd.nextInt(1 + r / 2);
            b = rnd.nextInt(1 + g);
        }
        return new int[] { r, g, b };
    }


}
