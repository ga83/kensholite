package com.beachcafesoftware.kensholite;

import com.badlogic.gdx.Net;

/**
 * Created by ubuntu on 5/09/18.
 */

public interface GdxAppUsageLogger extends Net.HttpResponseListener {

    void log() throws ParametersNotSetException;
    void setUrl(String url);
    void setInterval(int interval); // seconds
    void setApplicationName(String appname);
}
