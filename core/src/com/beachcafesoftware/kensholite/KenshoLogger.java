package com.beachcafesoftware.kensholite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;

import sun.rmi.runtime.Log;

/**
 * Created by ubuntu on 5/09/18.
 */

public class KenshoLogger implements GdxAppUsageLogger {

    String url = null;
    String applicationName = null;
    int interval = -1;
    static Thread threadNewest;    // key: thread. value: whether the thread needs to end.
    static Object lock = new Object();


    private Thread getStoredThread() {
        synchronized(lock) {
            return threadNewest;
        }
    }

    private void setNewestThread(Thread thread) {
        synchronized (lock) {
            System.out.println("aaa setting newest thread to " + thread);
            threadNewest = thread;
        }
    }

    @Override
    public void log() throws ParametersNotSetException {

        if (url == null | interval == -1 | applicationName == null) {
            throw new ParametersNotSetException();
        } else {
            final KenshoLogger kenshoLogger = this;

            Runnable loggerRunnable = new Runnable() {
                public void run() {

                   while (true) {
                        // if the stored thread is not this one, this one is out of date, so quit.
                        if(getStoredThread() != Thread.currentThread()) {
                            System.out.println("aaa this thread not newest, ending now " + getStoredThread() + " != " + Thread.currentThread());
                            return;
                        }

                       HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
                        Net.HttpRequest request = requestBuilder.newRequest().method(Net.HttpMethods.GET).url(url + "?application=" + applicationName + "&interval=" + interval).build();
                        request.setTimeOut(1000 * 20);

                        System.out.println("sending request");
                        Gdx.net.sendHttpRequest(request, kenshoLogger);

                        try {
                            Thread.sleep(1000 * interval);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
            Thread thread = new Thread(loggerRunnable);
            thread.start();
            setNewestThread(thread);
        }
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void setInterval(int interval) {
        this.interval = interval;
    }

    @Override
    public void setApplicationName(String appname) {
        this.applicationName = appname;
    }

    @Override
    public void handleHttpResponse(Net.HttpResponse httpResponse) {
//        String responseString = httpResponse.getResultAsString();
        System.out.println("got response");
    }

    @Override
    public void failed(Throwable t) {
        System.out.println("failed");
    }

    @Override
    public void cancelled() {
        System.out.println("cancelled");
    }
}
