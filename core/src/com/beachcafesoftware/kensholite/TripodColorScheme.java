package com.beachcafesoftware.kensholite;

import java.util.Random;

/**
 * Created by ubuntu on 21/09/16.
 */

public class TripodColorScheme implements ColorScheme
{
    private final Random rnd;


    public TripodColorScheme() {
        rnd = new Random();
    }

    @Override
    public int[] getColor()
    {
        float saturation = 0.99f;
        float lightness = 0.5f;
        float hue = rnd.nextFloat();

        hue += rnd.nextFloat() * 0.01f;

        int random = rnd.nextInt(3);
        if(random == 1) { hue += 0.33f; }
        if(random == 2) { hue += 0.66f; }

        if(hue > 1.0f) { hue -= 1.0f; }

        float[] rgb = hslToRgb(hue, saturation, lightness);

        int[] color = new int[] {(int) (rgb[0] * 256), (int) (rgb[1] * 256), (int) (rgb[2] * 256)};
        return color;
    }

    private static float[] hslToRgb(float h, float s, float l) {
        float r;
        float g;
        float b;

        if (s == 0) {
            r = g = b = l; // achromatic
        } else {
            float q = l < 0.5f ? l * (1 + s) : l + s - l * s;
            float p = 2 * l - q;
            r = hue2rgb(p, q, h + 1f / 3f);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1f / 3f);
        }
        return new float[] { r , g , b };
    }

    private static float hue2rgb(float p, float q, float t) {
        if (t < 0f) t += 1f;
        if (t > 1f) t -= 1f;
        if (t < 1f / 6f) return p + (q - p) * 6f * t;
        if (t < 1f / 2f) return q;
        if (t < 2f / 3f) return p + (q - p) * (2f / 3f - t) * 6f;
        return p;
    }

}
