package com.beachcafesoftware.kensholite;

import java.util.Random;

/**
 * Created by ubuntu on 18/12/17.
 */

public class RandomRGBColorScheme implements ColorScheme {

    private final Random rnd;


    public RandomRGBColorScheme() {
        rnd = new Random();
    }

    @Override
    public int[] getColor() {
        int[] color = Color.getRandomColorInteger();
        return color;
    }


}
