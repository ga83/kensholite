package com.beachcafesoftware.kensholite;

import com.badlogic.gdx.math.Vector2;

import java.util.Random;

/**
 * Created by ubuntu on 28/07/16.
 */
public class PointKensho {

    private final static float ANGULAR_VELOCITY = 90.0f;
    private final boolean dummy;
    private float frameTime;

    float x;
    float y;
    float size;
    Vector2 velocity;
    Vector2 fromCentreToPoint;


    public void updatePosition() {

        if(dummy) { return; }

        if(x > 1 || x < 0 || y > 1 || y < 0) {
            float angleToTurnInOneFrame = ANGULAR_VELOCITY * frameTime;
            fromCentreToPoint.set(x - 0.5f, y - 0.5f);

            float dotLeft  = velocity.cpy().rotate( angleToTurnInOneFrame).dot(fromCentreToPoint);
            float dotRight = velocity.cpy().rotate(-angleToTurnInOneFrame).dot(fromCentreToPoint);

//            System.out.println("dl " + dotLeft + ", dr " + dotRight);

            if(dotLeft < dotRight) {
                velocity.rotate(ANGULAR_VELOCITY * frameTime);
            }
            else {
                velocity.rotate(-ANGULAR_VELOCITY * frameTime);
            }
        }

        x += velocity.x;
        y += velocity.y;
    }

     public PointKensho(float x, float y, boolean dummy, int framesPerSecond) {
        this.x = x;
        this.y = y;
        this.dummy = dummy;
        this.frameTime = 1.0f / framesPerSecond;
    }

 //   public void calculateFrameTime(int framesPerSecond) {
   //     frameTime = 1.0f / framesPerSecond;
 //   }

    public PointKensho(float x, float y, float size, float movementSpeedV, boolean dummy, int framesPerSecond) {
        this.size = size;
        this.x = x;
        this.y = y;
        this.velocity = (new Vector2()).setToRandomDirection().scl(movementSpeedV);
        this.dummy = dummy;
        this.fromCentreToPoint = new Vector2();
        this.frameTime = 1.0f / framesPerSecond;
    }

}
