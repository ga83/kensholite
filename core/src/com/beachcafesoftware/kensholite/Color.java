package com.beachcafesoftware.kensholite;

import java.util.Random;

/**
 * Created by ubuntu on 23/08/17.
 */
public class Color {

    private static Random rnd = new Random();

    // constants are (start, magnitude-of-range)
    private static float[] RANGE_RED_GREEN = new float[] { 0f, 0.33f };
    private static float[] RANGE_YELLOW_CYAN = new float[] { 0f + 0.33f / 2f, 0.33f };
    private static float[] RANGE_GREEN_BLUE = new float[] { 0.33f, 0.33f };
    private static float[] RANGE_CYAN_MAGENTA = new float[] { 0.33f + 0.33f / 2f, 0.33f };
    private static float[] RANGE_BLUE_RED = new float[] { 0.66f, 0.33f };
    private static float[] RANGE_MAGENTA_YELLOW = new float[] { 0.66f + 0.33f / 2f, 0.33f };
    private static float[] RANGE_ALL = new float[] { 0f, 1.0f };


    private static String[] SCHEMES = new String[] { "redgreen","yellowcyan","greenblue","cyanmagenta","bluered", "magentayellow", "all" };


    public int r;
    public int g ;
    public int b ;


    public Color(int r,int g,int b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public Color(int r, int g, int b, int a) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public static float[] colorSchemeToArray(String scheme) {
        if(scheme.equals("random")) {
            scheme = SCHEMES[rnd.nextInt(SCHEMES.length)];
        }

        if(scheme.equals("redgreen")) { return RANGE_RED_GREEN; }
        if(scheme.equals("yellowcyan")) { return RANGE_YELLOW_CYAN; }
        if(scheme.equals("greenblue")) { return RANGE_GREEN_BLUE; }
        if(scheme.equals("cyanmagenta")) { return RANGE_CYAN_MAGENTA; }
        if(scheme.equals("bluered")) { return RANGE_BLUE_RED; }
        if(scheme.equals("magentayellow")) { return RANGE_MAGENTA_YELLOW; }
        if(scheme.equals("all")) { return RANGE_ALL; }

        return null;
    }

    public static float[] getRandomColorInRange(String colorParams) {
        float[] colorParameters = colorSchemeToArray(colorParams);
        float hue = colorParameters[0] + rnd.nextFloat() * colorParameters[1];
        if(hue > 1.0f) { hue -= 1.0f; }
        float saturation = 0.5f;
        float lightness = 0.075f * 0.0025f;
        return hslToRgb(hue, saturation, lightness);
    }

    public static float[] getRandomColor() {
        float[] colorParameters = new float[] { 0.0f, 1.0f };
        float hue = colorParameters[0] + rnd.nextFloat() * colorParameters[1];
        if(hue > 1.0f) { hue -= 1.0f; }
        float saturation = rnd.nextFloat();
        float lightness = rnd.nextFloat();
        return hslToRgb(hue, saturation, lightness);
    }

    public static int[] getRandomColorInteger() {
        float[] rgbColors = getRandomColor();
        return new int[] { (int) (rgbColors[0] * 256), (int) (rgbColors[1] * 256), (int) (rgbColors[2] * 256) };
    }

    public static float[] getRandomPastelColor() {
        float[] colorParameters = new float[] { 0.0f, 1.0f };
        float hue = colorParameters[0] + rnd.nextFloat() * colorParameters[1];
        if(hue > 1.0f) { hue -= 1.0f; }
        float saturation = 0.25f + rnd.nextFloat() * 0.7f;
        float lightness = 0.7f + rnd.nextFloat() * 0.15f;
        return hslToRgb(hue, saturation, lightness);
    }

    public static int[] getRandomPastelColorInteger() {
        float[] rgbColors = getRandomPastelColor();
        return new int[] { (int) (rgbColors[0] * 256), (int) (rgbColors[1] * 256), (int) (rgbColors[2] * 256) };
    }

    public static float[] getRandomDarkColor() {
        float[] colorParameters = new float[] { 0.0f, 1.0f };
        float hue = colorParameters[0] + rnd.nextFloat() * colorParameters[1];
        if(hue > 1.0f) { hue -= 1.0f; }
        float saturation = rnd.nextFloat();
        float lightness = 0.2f + rnd.nextFloat() * 0.25f;
        return hslToRgb(hue, saturation, lightness);
    }

    public static int[] getRandomDarkColorInteger() {
        float[] rgbColors = getRandomDarkColor();
        return new int[] { (int) (rgbColors[0] * 256), (int) (rgbColors[1] * 256), (int) (rgbColors[2] * 256) };
    }

    public static int[] getDarker(int[] color) {
        return new int[] { color[0] / 2, color[1] / 2, color[2] / 2 };
    }



    public static float[] hslToRgb(float h, float s, float l) {
        float r;
        float g;
        float b;

        if (s == 0) {
            r = g = b = l; // achromatic
        } else {
            float q = l < 0.5f ? l * (1 + s) : l + s - l * s;
            float p = 2 * l - q;
            r = hue2rgb(p, q, h + 1f / 3f);
            g = hue2rgb(p, q, h);
            b = hue2rgb(p, q, h - 1f / 3f);
        }
        return new float[] { r , g , b  };
    }

    private static float hue2rgb(float p, float q, float t) {
        if (t < 0f) t += 1f;
        if (t > 1f) t -= 1f;
        if (t < 1f / 6f) return p + (q - p) * 6f * t;
        if (t < 1f / 2f) return q;
        if (t < 2f / 3f) return p + (q - p) * (2f / 3f - t) * 6f;
        return p;
    }

    public static float[] rgbToHsl(float r, float g, float b){
        float max = getMax(r, g, b);
        float min = getMin(r, g, b);
        float h, s, l = (max + min) / 2f;

        if(max == min){
            h = s = 0; // achromatic
//            System.out.println("achro");
        }else{
            float d = max - min;
            s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

                if(max == r) {
                    h = (g - b) / d + (g < b ? 6 : 0);
                }
                else if(max == g) {
                    h = (b - r) / d + 2;
                 }
                else {
                    h = (r - g) / d + 4;
                 }
            h /= 6;
        }
        return new float[] { h, s, l };
    }

    public static float[] rgbToHsv(float r, float g, float b){

        float max = getMax(r, g, b);
        float min = getMax(r, g, b);
        float h, s, v = max;

        float d = max - min;
        s = max == 0 ? 0 : d / max;

        if(max == min){
            h = 0; // achromatic
        }else{
                if(max == r) { h = (g - b) / d + (g < b ? 6 : 0); }
                else if(max == g) { h = (b - r) / d + 2; }
                else { h = (r - g) / d + 4; }

            h /= 6;
        }

        return new float[] { h, s, v };
    }

    private static float getMax(float r, float g, float b) {
        return Math.max(Math.max(r,g),b);
    }

    private static float getMin(float r, float g, float b) {
        return Math.min(Math.min(r,g),b);
    }

}
